
class ListFromFile(object):
    """ Read a file containing a list of strings """
    def __init__(self, fileName):
        self.list = []
        self.__readFile(fileName)
    def __readFile(self,fileName):
        inputFile = open(fileName)
        line = inputFile.readline()
        while line:
            self.__addLine(line)
            line = inputFile.readline()
        inputFile.close()
    def __addLine(self,line):
        line = line.strip()
        if len(line) > 3:
            self.list.append(line)
    def getList(self):
        return self.list
