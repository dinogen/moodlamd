from flask import Flask
from flask import Flask, render_template
from LedController import LedController
from Led import Led

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')
    
@app.route('/seton/<int:lednum>')
def seton(lednum):
    ledController = LedController()
    led = ledController.getLed(lednum)
    led.setOn()
    return render_template('index.html')

@app.route('/setoff/<int:lednum>')
def setoff(lednum):
    ledController = LedController()
    led = ledController.getLed(lednum)
    led.setOff()
    return render_template('index.html')
    
@app.route('/settriplet/<color>/<int:value>')
def settriplet(color, value):
    ledController = LedController()
    if color == "red":
        ledTriplet = ledController.getRedTriplet()
    if color == "green":
        ledTriplet = ledController.getGreenTriplet()
    if color == "blue":
        ledTriplet = ledController.getBlueTriplet()
    ledTriplet.setValue(value)
    return "OK"
    
if __name__ == "__main__":
    app.run(host='0.0.0.0')