import urllib.request

class RssDownloader(object):
    """Download a url via internet and produce an xml string"""
    def __init__(self, url):
        self.url = url
    def getData(self):
        response = urllib.request.urlopen(self.url)
        self.data = response.read()
        return self.data
