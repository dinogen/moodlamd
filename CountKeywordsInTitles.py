class CountKeywordsInTitles(object):
    def __init__(self, keywordsList, titleList):
        self.keywordsList = keywordsList
        self.titleList = titleList
        self.count = 0
    def __isSomeKeywordInTitle(self,title):
        for word in self.keywordsList:
            if word in title:
                print("Found kw: %s." % word)
                return True
        return False
    def keywordCount(self):
        self.count = 0
        for title in self.titleList:
            if self.__isSomeKeywordInTitle(title):
                self.count = self.count + 1
        print("Counted %d words." % self.count)
        return self.count
