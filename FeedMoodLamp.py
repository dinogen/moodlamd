from RssDownloader import RssDownloader
from LedController import LedController
from LedTriplet import LedTriplet
from ListFromFile import ListFromFile
from ExtractTitles import ExtractTitles
from CountKeywordsInTitles import CountKeywordsInTitles
from ColorCalculator import ColorCalculator

class FeedMoodLamp(object):
    def __init__(self, listFromFile_rss, listFromFile_red, listFromFile_green, listFromFile_blue):
        self.rssList     = listFromFile_rss.getList()
        self.redKwList   = listFromFile_red.getList()
        self.greenKwList = listFromFile_green.getList()
        self.blueKwList  = listFromFile_blue.getList()
        self.__buildTriplets()
        self.updateFeed()
        
    def updateFeed(self):
        self.__buildTitleList()
        self.__countKws()

    def __buildTitleList(self):
        self.titleList = []
        for rssUrl in self.rssList:
            rssDownloader = RssDownloader(rssUrl)
            data = rssDownloader.getData()
            extractTitles = ExtractTitles(data)
            self.titleList = self.titleList + extractTitles.getTitlesList()

    def __countKws(self):
        countRedKeywordsInTitles   = CountKeywordsInTitles(self.redKwList, self.titleList)
        countGreenKeywordsInTitles = CountKeywordsInTitles(self.greenKwList, self.titleList)
        countBlueKeywordsInTitles  = CountKeywordsInTitles(self.blueKwList, self.titleList)
        self.redKwNumber   = countRedKeywordsInTitles.keywordCount()
        self.greenKwNumber = countGreenKeywordsInTitles.keywordCount()
        self.blueKwNumber  = countBlueKeywordsInTitles.keywordCount()

    def __buildTriplets(self):
        self.ledController   = LedController()
        self.ledTripletRed   = self.ledController.getRedTriplet()
        self.ledTripletGreen = self.ledController.getGreenTriplet()
        self.ledTripletBlue  = self.ledController.getBlueTriplet()
    
    def setLedsAccordingToFeed(self):
        colorCalculator = ColorCalculator(self.redKwNumber, self.greenKwNumber, self.blueKwNumber)
        r,g,b = colorCalculator.getColor()
        self.ledTripletRed.setValue(r)
        self.ledTripletGreen.setValue(g)
        self.ledTripletBlue.setValue(b)

if __name__ == "__main__":
    listFromFile_rss = ListFromFile("rsslist.txt")
    listFromFile_red = ListFromFile("red.txt")
    listFromFile_green = ListFromFile("green.txt")
    listFromFile_blue = ListFromFile("blue.txt")
    feedMoodLamp = FeedMoodLamp(listFromFile_rss, listFromFile_red, listFromFile_green, listFromFile_blue)
    feedMoodLamp.setLedsAccordingToFeed()
    
