import RPi.GPIO as GPIO

class Led(object):
    def __init__(self, pin):
        self.pin = pin
        GPIO.setup(pin, GPIO.OUT)
        self.setOff()

    def setOn(self):
        GPIO.output(self.pin, GPIO.HIGH)
        self.status = True
    
    def setOff(self):
        GPIO.output(self.pin, GPIO.LOW)
        self.status = False

if __name__ == "__main__":
    # test
    from LedController import LedController
    import time
    ledController = LedController()
    led = Led(26) # rosso 1
    for i in range(10):
        led.setOn()
        time.sleep(1)
        led.setOff()
        time.sleep(1)
