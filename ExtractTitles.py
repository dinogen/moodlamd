from xml.dom.minidom import parseString

class ExtractTitles:
    def __init__(self,rssString):
        self.rssString = rssString
        self.titlesList = []
        self.__extractTitlesFrom(rssString)
    def getTitlesList(self):
        return self.titlesList
    def __extractTitlesFrom(self,rssString):
        dom1 = parseString(rssString)
        itemsNodeList = dom1.getElementsByTagName("item")
        for item in itemsNodeList:
            titleNodeList = item.getElementsByTagName("title")
            for title in titleNodeList:
                for node in title.childNodes:
                    if node.nodeType == node.TEXT_NODE:
                        self.titlesList.append(node.nodeValue)
