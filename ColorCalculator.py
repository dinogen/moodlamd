class ColorCalculator(object):
    def __init__(self, red, green, blue):
        self.red = red
        self.green = green
        self.blue = blue
        self.doTheMath()

    def doTheMath(self):
        print("ColorCalculator.doTheMath: input %d %d %d" % (self.red, self.green, self.blue))
        if (self.red == self.green and self.green == self.blue):
            self.red = 3
            self.green = 3
            self.blue = 3
        else:
            maxx = max(self.red, self.green, self.blue)
            self.red = int(self.red * 3 / maxx)
            self.green = int(self.green * 3 / maxx)
            self.blue = int(self.blue * 3 / maxx)
        print("ColorCalculator.doTheMath: output %d %d %d" % (self.red, self.green, self.blue))

    def getColor(self):
        return (self.red,self.green,self.blue)
