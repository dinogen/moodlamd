import unittest
from ListFromFile import ListFromFile
from ExtractTitles import ExtractTitles
from RssDownloader import RssDownloader
from CountKeywordsInTitles import CountKeywordsInTitles
from ColorCalculator import ColorCalculator

class TestListFromFile(unittest.TestCase):
    def test_read(self):
        listFromFile = ListFromFile("test.txt")
        self.assertEqual(listFromFile.getList(), ['cani','gatti','canarini'])

class TestExtractTitles(unittest.TestCase):
    def test_1(self):
        extractTitles = ExtractTitles("""<rss>
         <item>
           <title>t1</title>
         </item>
         <item>
           <title>t2</title>
         </item>
         <item>
           <title>t3</title>
         </item>
        </rss>""")
        self.assertEqual(extractTitles.getTitlesList(), ['t1','t2','t3'])

# class TestRssDownloader(unittest.TestCase):
#     def test_1(self):
#         rssDownloader = RssDownloader("http://www.repubblica.it/rss/scienze/rss2.0.xml")
#         print(rssDownloader.getData())

class TestCountKeywordsInTitles(unittest.TestCase):
    def test_keywordCount(self):
        keywords = ['uno','due','tre','quattro']
        titles = ['a','b','c','d']
        countKeywordsInTitles = CountKeywordsInTitles(keywords, titles)
        self.assertEqual(countKeywordsInTitles.keywordCount(), 0)

        keywords = ['uno','due','tre','quattro']
        titles = ['a','b due','c','d']
        countKeywordsInTitles = CountKeywordsInTitles(keywords, titles)
        self.assertEqual(countKeywordsInTitles.keywordCount(), 1)

        keywords = ['uno','due','tre','quattro']
        titles = ['a','b due','c','d tre quattro']
        countKeywordsInTitles = CountKeywordsInTitles(keywords, titles)
        self.assertEqual(countKeywordsInTitles.keywordCount(), 2)

class TestColorCalculator(unittest.TestCase):
    def test_even(self):
        colorCalculator = ColorCalculator(0,0,0)
        self.assertEqual(colorCalculator.getColor(),(3,3,3))
        colorCalculator = ColorCalculator(5,5,5)
        self.assertEqual(colorCalculator.getColor(),(3,3,3))
    def test_red(self):
        colorCalculator = ColorCalculator(15,2,5)
        self.assertEqual(colorCalculator.getColor(),(3,0,1))

if __name__ == '__main__':
    unittest.main()
