import RPi.GPIO as GPIO

class LedTriplet(object):
    def __init__(self, pin1, pin2, pin3, ledController):
        self.ledController = ledController
        if ledController.ready:
            self.leds = (ledController.getLed(pin1), \
                         ledController.getLed(pin2), \
                         ledController.getLed(pin3) )

    def setLed(self, ledNumber, status):
        """ leds have numbers 0,1,2 """
        if self.ledController.ready:
            if ledNumber >= 0 and ledNumber <= 2:
                if status:
                    self.leds[ledNumber].setOn()
                else:
                    self.leds[ledNumber].setOff()

    def setValue(self, value):
        if self.ledController.ready:
            self.setLed(0, False if value < 1 else True)
            self.setLed(1, False if value < 2 else True)
            self.setLed(2, False if value < 3 else True)
            
if __name__ == "__main__":
    # test
    from LedController import LedController
    import time
    ledController = LedController()
    ledTriplet = LedTriplet(27,17,4, ledController) # blue leds
    for i in range(4):
        ledTriplet.setValue(0)
        time.sleep(1)
        ledTriplet.setValue(1)
        time.sleep(1)
        ledTriplet.setValue(2)
        time.sleep(1)
        ledTriplet.setValue(3)
        time.sleep(1)
    
