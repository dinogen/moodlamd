import RPi.GPIO as GPIO
from LedTriplet import LedTriplet
from Led import Led

class LedController(object):
    r1 = 26
    r2 = 19
    r3 = 13
    g1 = 6
    g2 = 5
    g3 = 22
    b1 = 27
    b2 = 17
    b3 = 4
    def __init__(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        self.ready = True
    def getRedTriplet(self):
        return LedTriplet(self.r1,self.r2,self.r3, self)
    def getGreenTriplet(self):
        return LedTriplet(self.g1,self.g2,self.g3, self)
    def getBlueTriplet(self):
        return LedTriplet(self.b1,self.b2,self.b3, self)
    def getLed(self, pin):
        return Led(pin)
    def getTriplet(self, pin1, pin2, pin3):
        return LedTriplet(pin1, pin2, pin3, self)
        
